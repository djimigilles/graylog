# graylog
## _Just for the interview_


## Build the deployment image

Go to the docker directory and build the deployment image

```sh 
cd docker
docker build -t registry.gitlab.com/djimigilles/graylog:base .
docker push registry.gitlab.com/djimigilles/graylog:base

````

[![asciicast](https://asciinema.org/a/VJcZFNXomWNPrhDSFFiq6oOli.svg)](https://asciinema.org/a/VJcZFNXomWNPrhDSFFiq6oOli?autoplay=1)

the pipeline will use a custom image base on `alpine:3.15.0` with terraform install.
Why using alpine ? 
Because its very lightweight.
Why not using terraform docker image ? 
Because with custom image we can futher add some tools like the aws cli or other tools according to our needs


## Configure the AWS Provider

https://docs.aws.amazon.com/cli/latest/userguide/cli-configure-profiles.html


## Deploy the infrastructure

run the pki script and copy the public key in the variable pair_key as following

[![asciicast](https://asciinema.org/a/Y5P55BlYl6Fd4dbUeBhqSxGXs.svg)](https://asciinema.org/a/Y5P55BlYl6Fd4dbUeBhqSxGXs?autoplay=1)

! be sure to set the variable with a public key

change the variable `backend_name`to an already existing S3 on which your credentials have access

#
#
#

### 1.Automatic deployment
The deployment of the infrastrucre is totally automated. The gitlab-ci pipeline is trigger at each push into the main branch.
However few variables need to be defined
  - AWS_ACCESS_KEY_ID
  - AWS_SECRET_ACCESS_KEY

![Screenshot](gitlab-ci-variables.png)

the `provider.profile` in `terraform/provider.tf` should match the acces and secret key.


### 2.Manual deployment

### 2.a Deployment

the aws credentials variable should be declared

```sh
export AWS_ACCESS_KEY_ID=""
export AWS_SECRET_ACCESS_KEY=""
```

then type:

```sh
terraform init
terraform plan
terraform apply
```

[![asciicast](https://asciinema.org/a/3ccaBhSYxdYQOJIWNLkQe1qB0.svg)](https://asciinema.org/a/3ccaBhSYxdYQOJIWNLkQe1qB0?autoplay=1)



### 2.b Test the application

curl to the loadbalancer public dns name

```sh
curl -i http://loadbalancerdnsname:80
```


### 2.c Clean-up

```sh
terraform destroy 
```


# INFRASTRUCTURE DIAGRAM
![Screenshot](graylog.drawio.png)