#!/bin/bash

set -x

PRIVATE_KEY_NAME="private_key.pem"
PRIVATE_KEY_BITS="2048"

PUBLIC_KEY_NAME="public_key.pem"


############################################################## KEYS ##############################################################
##
##
############################################################## KEYS ##############################################################

generate_private_key () {
    openssl genrsa -out ${PRIVATE_KEY_NAME} ${PRIVATE_KEY_BITS}
}


generate_public_key () {
    openssl rsa -in ${PRIVATE_KEY_NAME} -outform PEM -pubout -out ${PUBLIC_KEY_NAME}
}


generate_private_key
generate_public_key

