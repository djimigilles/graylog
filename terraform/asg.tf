######## RESOURCE: Auto Scaling Group
######## https://docs.aws.amazon.com/autoscaling/ec2/userguide/AutoScalingGroup.html
######## https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/autoscaling_group



resource "aws_autoscaling_group" "app_asg" {
  name               = "app-asg"
  #force_delete              = true
  vpc_zone_identifier = aws_subnet.public_subnet.*.id
  desired_capacity   = 3
  max_size           = 3
  min_size           = 1

  launch_template {
    id      = aws_launch_template.app_launch_template.id
    version = aws_launch_template.app_launch_template.latest_version 
  }

  instance_refresh {
    strategy = "Rolling"
    preferences {
      min_healthy_percentage = 50
    }
  }
}

resource "aws_autoscaling_attachment" "asg_attachment_bar" {
  autoscaling_group_name = aws_autoscaling_group.app_asg.id
  alb_target_group_arn   = aws_lb_target_group.app_tg.arn
}