######## RESOURCE: Elastic IP
######## https://docs.aws.amazon.com/AWSEC2/latest/UserGuide/elastic-ip-addresses-eip.html
######## https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/eip



resource "aws_eip" "eip" {
  count = length(var.paris_az)
  vpc = true

  tags = {
    Name = "${var.paris_az[count.index]}-eip"
  }

  depends_on = [aws_internet_gateway.principal_igw]
}