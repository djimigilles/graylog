#!/bin/bash
apt update
apt install -y nginx

cat > /etc/nginx/sites-enabled/default << "EOF"

server {
    listen 80 default_server;
    listen [::]:80 default_server;

    root /var/www/html;

    index index.html index.htm index.nginx-debian.html;

    server_name _;

    location / {
        return 200 "Hello Graylog!";
    }
}
EOF
nginx -s reload